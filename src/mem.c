#define _DEFAULT_SOURCE
#define BLOCK_MIN_CAPACITY 24

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {

    size_t region_size = region_actual_size(offsetof(struct block_header, contents) + query);
    void *region_address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, region_size, 0);
    }
    if (region_address == MAP_FAILED){
      return REGION_INVALID;
    }
   
    struct region reg = {
            reg.addr = region_address,
            reg.size = region_size,
            reg.extends = region_address == addr
    };

  block_init(region_address, (block_size){.bytes = region_size}, NULL);

  return reg;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header* block = (struct block_header*) HEAP_START;
  size_t size_for_free = 0;
  struct block_header* first_to_unmap = block;
  while(block) {
    struct block_header* next = block->next;
    size_for_free += size_from_capacity(block->capacity).bytes;
    if (!blocks_continuous(block, next)) {
      if (munmap(first_to_unmap, size_for_free) != 0) {
                perror("Ошибка munmap");
            }
      munmap(first_to_unmap, size_for_free);
      size_for_free = 0;
      first_to_unmap = next;
      }
    block = next;
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (!block_splittable(block, query)) {
        return false;
    }
    size_t split_size = size_max(query, BLOCK_MIN_CAPACITY);

    void *this_block = (void *)block;
    block_size this_block_size = size_from_capacity((block_capacity){split_size});

    void *further_block = this_block + this_block_size.bytes;
    block_size further_block_size = (block_size){block->capacity.bytes - split_size};

    void *next_block = block->next;

    block_init(this_block, this_block_size, further_block);
    block_init(further_block, further_block_size, next_block);

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next_block = block->next;

  if (next_block == NULL) {
    return false;
  }
  if (!mergeable(block,next_block)){
    return false;
  }

  block->capacity = (block_capacity){.bytes = block->capacity.bytes + size_from_capacity(next_block->capacity).bytes};
  block->next = next_block->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

enum search_status {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED};

struct block_search_result {
  enum search_status type;
  struct block_header* block;
};

static struct block_search_result form_result (enum search_status search_type, struct block_header* last_block) {
  struct block_search_result result = {
         result.type = search_type,
         result.block = last_block
        };
  return result;
}


static struct block_search_result find_good_or_last  ( struct block_header* block, size_t sz )    {
  struct block_header* last_block = NULL;
  enum search_status search_type = BSR_REACHED_END_NOT_FOUND;
  
  while(block) {
    while(try_merge_with_next(block)){};
    if (block->is_free && block_is_big_enough(sz, block)) {
      search_type = BSR_FOUND_GOOD_BLOCK;
      last_block = block;
      return form_result(search_type, last_block);
    }
    last_block = block;
    block = block->next;
  }

  return form_result(search_type, last_block);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result result = find_good_or_last(block, query);
  if(result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return result;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) return NULL;
    void* heap_start = block_after(last);
    struct region allocated_region = alloc_region(heap_start, query);
    if (region_is_invalid(&allocated_region)) return NULL;
    last->next = allocated_region.addr;

    if (allocated_region.extends && try_merge_with_next(last)) return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  size_t query_size = size_max(query, BLOCK_MIN_CAPACITY);
  if (heap_start == NULL) return NULL;

  struct block_search_result memalloc_state = try_memalloc_existing(query_size, heap_start);
    enum search_status m_type = memalloc_state.type;
    struct block_header* m_block = memalloc_state.block;

    if (m_type == BSR_FOUND_GOOD_BLOCK) return m_block;
    if (m_type == BSR_CORRUPTED) return NULL;

    struct block_header *created_header = grow_heap(m_block, query_size);

    if (created_header == NULL) {
      return NULL;
    } else {
      struct block_search_result memalloc_state_after_growth = try_memalloc_existing(query_size, created_header);
      struct block_header* m_a_g_block = memalloc_state_after_growth.block;
      return m_a_g_block;
    }
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
   while (try_merge_with_next(header));
}
